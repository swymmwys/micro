/*
 Name:		Lab1_3.ino
 Created:	1/31/2018 11:34:08 AM
 Author:	RRR
*/
const int button = 12;
const int led = 11;
int lastState = 0;
bool isLedEnabled = false;
// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(led, OUTPUT);
	pinMode(button, INPUT);
}

// the loop function runs over and over again until power down or reset
void loop() {
	int state = 0;

	state = digitalRead(button);
	if (state == LOW)
		return;
	delay(100);
	if (state == digitalRead(button))
		return;
	SwitchLed();
}

void SwitchLed()
{
	if (isLedEnabled)
	{
		digitalWrite(led, LOW);
		isLedEnabled = false;
	}
	else
	{
		digitalWrite(led, HIGH);
		isLedEnabled = true;
	}
}
