const int length = 16;
char mass[16] = {};
volatile int index = 0;


// the setup function runs once when you press reset or power the board
short pins[] = { 11,10,9 };
short Intervals[] = { 500, 1000, 1500 };
volatile bool values[] = { false, false, false };
short portsLength = 3;
volatile bool flag = false;
const int button = 2;

volatile int prevVal = 0;
volatile unsigned long prevTimer = 0;
volatile bool isShow = false;

void Output()
{
	for (int i = 0; i < index; i++)
	{
		Serial.print(mass[i]);
	}
	Serial.println();
}

void isr() {
	int currentVal = digitalRead(button);
	if (currentVal == 1)
	{
		isShow = true;
	}
	else
	{
		isShow = false;
	}
	if (currentVal != prevVal)
	{
		if (currentVal == 0)
		{
			unsigned long currentLong = millis();
			if ((currentLong - prevTimer) > 200)
				mass[index++] = '-';
			else
				mass[index++] = '.';
			prevTimer = currentLong;
			Output();
			if (index == 16)
				index = 0;
		}
		else
		{
			prevTimer = millis();
		}
	}
	prevVal = currentVal;
}

void setup() {
	attachInterrupt(0, isr, CHANGE);
	pinMode(13, OUTPUT);
	Serial.begin(9600);
}

void loop() {
	digitalWrite(13, isShow);
}
