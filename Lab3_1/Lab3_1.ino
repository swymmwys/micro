/*
Name:		Sketch2.ino
Created:	08.02.2018 19:16:39
Author:	tutba
*/

// the setup function runs once when you press reset or power the board
short pins[] = { 11,10,9 };
short portsLength = 3;
volatile bool flag = false;
void isr() {
	flag = !flag;
}

void setup() {
	attachInterrupt(0, isr, 2);
	pinMode(13, OUTPUT);
	for (short i = 0; i<portsLength; i++)
		pinMode(pins[i], OUTPUT);
}

// the loop function runs over and over again until power down or reset
short i = 0;
void loop() {
	digitalWrite(13, flag);

	digitalWrite(pins[i], HIGH);
	delay(500);
	digitalWrite(pins[i], LOW);
	i++;
	if (i >= portsLength) {
		i = 0;
	}
}
