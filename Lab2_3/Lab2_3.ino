/*
Name:		Lab2_2.ino
Created:	2/7/2018 9:33:39 AM
Author:	RRR
*/

const int potPort = 3;
const int button1 = 0;
const int button2 = 1;
const int button3 = 2;
const int led1 = 11;
const int led2 = 10;
const int led3 = 9;
bool isLed1 = false;
bool isLed2 = false;
bool isLed3 = false;

bool isSwitched1;
bool isSwitched2;
bool isSwitched3;


int ledValue = 128;

// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(led1, OUTPUT);
	pinMode(led2, OUTPUT);
	pinMode(led3, OUTPUT);
	pinMode(button1, INPUT);
	Serial.begin(9600);
}

// the loop function runs over and over again until power down or reset
void loop() {
	ReadCurrentValue();
	ReadButtonsValue();
	OutputValue();
	delay(100);
}

void ReadButtonsValue()
{
	ReadButton1();
	ReadButton2();
	ReadButton3();	
}

void ReadButton1()
{
	int val1 = analogRead(button1);
	if (val1 > 500)
	{
		if (isSwitched1)
			return;
		isLed1 = !isLed1;
		Serial.println("Button 1");
		isSwitched1 = true;
	}
	else
		isSwitched1 = false;
}

void ReadButton2()
{
	int val2 = analogRead(button2);
	if (val2 > 500)
	{
		if (isSwitched2 == true)
			return;
		isLed2 = !isLed2;
		Serial.println("Button 2");
		isSwitched2 = true;
	}
	else
		isSwitched2 = false;
}

void ReadButton3()
{
	int val3 = analogRead(button3);
	if (val3 > 500)
	{
		if (isSwitched3 == true)
			return;
		isLed3 = !isLed3;
		Serial.println("Button 3");
		isSwitched3 = true;
	}
	else
		isSwitched3 = false;
}

void OutputValue()
{
	if (isLed1)
		analogWrite(led1, ledValue);
	else
		analogWrite(led1, 0);
	
	if (isLed2)
		analogWrite(led2, ledValue);
	else
		analogWrite(led2, 0);
	
	if (isLed3)
		analogWrite(led3, ledValue);
	else
		analogWrite(led3, 0);
}

void ReadCurrentValue()
{
	int value = analogRead(potPort);
	float step = value / 1024.0;
	int newLedValue = step * 256;
	if (newLedValue != ledValue)
	{
		Serial.print("Value = ");
		Serial.println(value);
	}
	ledValue = newLedValue;
}