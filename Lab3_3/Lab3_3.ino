/*
Name:		Sketch2.ino
Created:	08.02.2018 19:16:39
Author:	tutba
*/

// the setup function runs once when you press reset or power the board
short pins[] = { 11,10,9 };
short Intervals[] = { 500, 1000, 1500 };
volatile bool values[] = { false, false, false };
short portsLength = 3;
volatile bool flag = false;
void isr() {
	for (int i = 0; i < portsLength; i++)
		values[i] = !values[i];
}

void setup() {
	attachInterrupt(0, isr, 2);
	pinMode(13, OUTPUT);
	for (short i = 0; i<portsLength; i++)
		pinMode(pins[i], OUTPUT);
}

// the loop function runs over and over again until power down or reset
short i = 0;
void loop() {
	
	unsigned long elapsed = millis();
	for (int i = 0; i < portsLength; i++)
	{
		if (elapsed % Intervals[i] == 0)
			values[i] = !values[i];
		digitalWrite(pins[i], values[i]);
	}
}
